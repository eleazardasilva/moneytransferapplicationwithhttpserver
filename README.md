<h1 align="center">  
  <br>
   Money transfer APP
  <br>
</h1>
<h4 align="center">An implementation of a money transaction application using <a href="https://docs.oracle.com/javase/8/docs/jre/api/net/httpserver/spec/com/sun/net/httpserver/HttpServer.html"
 target="_blank">HttpServer</a>.</h4>

<p align="center">
  <a href="#intro">Intro</a> •
  <a href="#project-features">Project Features</a> •
  <a href="#how-to-use">How To Use</a> •
  <a href="#license">License</a> 
</p>

## Intro

This is a small project to showcase the use of a simple HttpServer in the implementation of a web server in java.

## Project Features

* Server side application uses a HttpServer with fixedThreadPool to handle multiple concurrent requests.
* There is a persistence interface to define the interaction with the database.
* For simplicity the database is implemented as a simple class with lists. I intend to later change it to a actual database (mongoDB, PostgreSQL or simply SQLite)
* In this database I use java streams to demonstrate the implementation of a parallel collector.

## How To Use

Clone the project.
There is a test class with multiple tests that can validate the functionality.
You can also make requests to the server using a client (Postman or other of your choice)
The server is configured to run on port 9876.
To clone and run this application, you'll need [Git](https://git-scm.com) and a java idea. 

From your command line:

```bash
# Clone this repository
$ git clone https://eleazardasilva@bitbucket.org/eleazardasilva/moneytransferapplicationwithgrpc.git

# Open the IDEA and open the project
# Two options to run the app:
## Run the unit tests
## Start the server and send messages with your own client.
## The base uri is /transaction/v1
## At this stage there are 3 operations implemented:
* getUserInfo
* getUserBalance
* processTransaction
e.g. curl -L -X GET 'http://localhost:9876/transaction/v1/getUserInfo?name=carlos123'
reply
{
    "name": "Carlos",
    "userName": "carlos123",
    "balance": {
        "EUR": 344.32,
        "USD": 450.09,
        "BTC": 1.000000000,
        "JPY": 50.09
    }
}
```

## License

MIT

---
<h1 align="center">
  <a href="https://bitbucket.org/eleazardasilva/moneytransferapplicationwithsockets/raw/4e58cb9b59d6787af014e0747dcc4b1cc61c4269/prints/get_1.JPG" alt="get" width="900"></a>
</h1>