package com.eleazar.backend.example.controller;

import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Eleazar da Silva on 15-11-2022.
 * Socket implementation.
 *
 */

public class TransactionServiceServer {
    public static void main(String[] args) throws IOException {
        HttpServer server = HttpServer.create(new InetSocketAddress("localhost", 9876), 0);
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        server.createContext("/transaction/v1", new serverHttpHandler());
        server.setExecutor(executorService);
        server.start();
        System.out.println("Server started on port 9876");
    }
}



