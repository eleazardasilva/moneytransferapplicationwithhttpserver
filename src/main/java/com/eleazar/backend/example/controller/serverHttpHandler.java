package com.eleazar.backend.example.controller;

import com.eleazar.backend.example.entity.Transaction;
import com.eleazar.backend.example.entity.TransactionReply;
import com.eleazar.backend.example.entity.User;
import com.eleazar.backend.example.service.TransactionProcessor;
import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;

class serverHttpHandler implements HttpHandler {
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        System.out.println("new request");
        // Retrieve request parameters
        String method = httpExchange.getRequestMethod();
        String uri = httpExchange.getRequestURI().toString().contains("?")
                ? httpExchange.getRequestURI().toString().split("\\?")[0]
                : httpExchange.getRequestURI().toString();
        String params = httpExchange.getRequestURI().toString().contains("?")
                ? httpExchange.getRequestURI().toString().split("\\?")[1] : "";
        String operation = uri.split("/")[3];
        String body = httpExchange.getRequestBody().available() > 0 ? handleBody(httpExchange.getRequestBody()) : "";
        // Request method
        switch (method) {
            case "GET" :
                System.out.println("get");
                handleGetRequest(httpExchange, operation, params);
                break;
            case "POST" :
                System.out.println("post");
                handlePostRequest(httpExchange,operation,body);
                break;
            case "PUT":
                System.out.println("put");
                break;
            case "DELETE":
                System.out.println("delete");
                break;
            default:
                System.out.println("invalid method");
        }
    }

    private void handlePostRequest(HttpExchange httpExchange, String operation, String body) throws  IOException{
        System.out.println(operation);
        if ("processTransaction".equals(operation)) {
            TransactionProcessor processor = new TransactionProcessor();
            // Parse json body google json library
            Gson gson = new Gson();
            Transaction transaction = gson.fromJson(body, Transaction.class);
            System.out.println(transaction);
            // make transaction request
            TransactionReply reply = processor.processTransference(transaction);
            System.out.println(reply);

            // create response objects
            OutputStream outputStream = httpExchange.getResponseBody();
            // write response
            String response = gson.toJson(reply);
            // set important headers
            httpExchange.getResponseHeaders().add("Content-Type","application/json");
            // calculate status code
            if (reply.getCode() == 0) {
                httpExchange.sendResponseHeaders(200, response.length());
            } else {
                httpExchange.sendResponseHeaders(422, response.length());
            }
            outputStream.write(response.getBytes());
            outputStream.flush();
            outputStream.close();
        }
    }

    private void handleGetRequest(HttpExchange httpExchange, String operation, String params) throws  IOException{
        System.out.println(operation);
        TransactionProcessor processor = new TransactionProcessor();
        if ("getUserBalance".equals(operation)) {
            String userName = params.split("&")[0].split("=")[1];
            String currency = params.split("&")[1].split("=")[1];
            BigDecimal balance = processor.getUserBalance(userName, User.currency.valueOf(currency));

            // create response objects
            OutputStream outputStream = httpExchange.getResponseBody();
            String response = "The user balance is " + balance + currency;
            // set important headers
            httpExchange.sendResponseHeaders(200, response.length());

            outputStream.write(response.getBytes());
            outputStream.flush();
            outputStream.close();
        } else if ("getUserInfo".equals(operation)) {
            String userName = params.split("&")[0].split("=")[1];
            User user = processor.getUserInfo(userName);

            Gson gson = new Gson();
            OutputStream outputStream = httpExchange.getResponseBody();
            String response = gson.toJson(user);

            httpExchange.getResponseHeaders().add("Content-Type","application/json");
            httpExchange.sendResponseHeaders(200, response.length());

            outputStream.write(response.getBytes());
            outputStream.flush();
            outputStream.close();
        } else if ("getUserTransactions".equals(operation)) {
            String userName = params.split("&")[0].split("=")[1];
            ArrayList<Transaction> transactions = processor.getUserTransactions(userName);

            Gson gson = new Gson();
            // concat transaction list into a string
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            for (Transaction transaction : transactions) {
                sb.append(gson.toJsonTree(transaction));
            }
            sb.append("]");
            OutputStream outputStream = httpExchange.getResponseBody();
            String response = sb.toString();

            httpExchange.getResponseHeaders().add("Content-Type", "application/json");
            httpExchange.sendResponseHeaders(200, response.length());

            outputStream.write(response.getBytes());
            outputStream.flush();
            outputStream.close();
        } else {
            TransactionReply reply = TransactionReply.Builder.newInstance()
                    .setCode(6)
                    .setDescription("Operation not found")
                    .build();
            Gson gson = new Gson();
            OutputStream outputStream = httpExchange.getResponseBody();
            String response = gson.toJson(reply);

            httpExchange.getResponseHeaders().add("Content-Type", "application/json");
            httpExchange.sendResponseHeaders(400, response.length());

            outputStream.write(response.getBytes());
            outputStream.flush();
            outputStream.close();
        }
    }

    private String handleBody(InputStream inputStream) {
        StringBuilder sb = new StringBuilder();
        System.out.println("handle body");
        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ( (line = br.readLine()) != null) {
                sb.append(line).append(System.lineSeparator());
            }
            String content = sb.toString();
            //as example, you can see the content in console output
            System.out.println(content);
            return content;
        } catch (IOException e) {
            e.printStackTrace();
            return "error";
        }
    }
}