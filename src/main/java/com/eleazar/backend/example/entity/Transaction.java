package com.eleazar.backend.example.entity;

import java.math.BigDecimal;

/**
 * Created by Eleazar da Silva on 13-11-2022.
 * Defines the transaction in the system
 */
public class Transaction {

    private int transactionId;
    private String userNameFrom;
    private String userNameTo;
    private BigDecimal amount;
    private User.currency currency;
    private String timestamp;

    public int getTransactionId() {
        return transactionId;
    }

    public String getUserNameFrom() {
        return userNameFrom;
    }

    public String getUserNameTo() {
        return userNameTo;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public User.currency getCurrency() {
        return currency;
    }

    public String getTimestamp() {
        return timestamp;
    }

    private Transaction(Builder builder) {
        this.transactionId = builder.transactionId;
        this.userNameFrom = builder.userNameFrom;
        this.userNameTo = builder.userNameTo;
        this.amount = builder.amount;
        this.currency = builder.currency;
        this.timestamp = builder.timestamp;
    }

    public void setTransactionId(int transactionId) {
        this.transactionId = transactionId;
    }

    public static class Builder {

        private int transactionId;
        private String userNameFrom;
        private String userNameTo;
        private BigDecimal amount;
        private User.currency currency;
        private String timestamp;

        public static Builder newInstance() {
            return new Builder();
        }

        private Builder() {}

        public Builder setTransactionId(int transactionId) {
            this.transactionId = transactionId;
            return this;
        }

        public Builder setUserNameFrom(String userNameFrom) {
            this.userNameFrom = userNameFrom;
            return this;
        }

        public Builder setUserNameTo(String userNameTo) {
            this.userNameTo = userNameTo;
            return this;
        }

        public Builder setAmount(BigDecimal amount) {
            this.amount = amount;
            return this;
        }

        public Builder setCurrency(User.currency currency) {
            this.currency = currency;
            return this;
        }

        public Builder setTimestamp(String timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public Transaction build() {
            return new Transaction(this);
        }
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "transactionId=" + transactionId +
                ", userNameFrom='" + userNameFrom + '\'' +
                ", userNameTo='" + userNameTo + '\'' +
                ", amount=" + amount +
                ", currency=" + currency +
                ", timestamp='" + timestamp + '\'' +
                '}';
    }
}
