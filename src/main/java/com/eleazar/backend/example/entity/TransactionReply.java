package com.eleazar.backend.example.entity;

public class TransactionReply {

    private int code;
    private String description;

    private TransactionReply(Builder builder) {
        this.code = builder.code;
        this.description = builder.description;
    }

    public int getCode() {
        return code;
    }

    public static class Builder {

        private int code;
        private String description;

        public static Builder newInstance() {
            return new Builder();
        }

        private Builder() {}

        public Builder setCode(int code) {
            this.code = code;
            return this;
        }

        public Builder setDescription(String description) {
            this.description = description;
            return this;
        }

        public TransactionReply build() {
            return new TransactionReply(this);
        }
    }

    @Override
    public String toString() {
        return "TransactionReply{" +
                "code=" + code +
                ", description='" + description + '\'' +
                '}';
    }
}
