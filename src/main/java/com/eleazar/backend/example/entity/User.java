package com.eleazar.backend.example.entity;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Eleazar da Silva on 13-11-2022.
 * Defines the user in the system
 */
public class User {

    private String name;
    private String userName;

    public enum currency {
        GBD,
        EUR,
        USD,
        JPY,
        CAD,
        BTC
    }
    private Map<currency, BigDecimal> balance;

    public String getName() {
        return name;
    }

    public String getUserName() {
        return userName;
    }

    public BigDecimal getBalance(currency currency) {
        return balance.getOrDefault(currency, new BigDecimal(0.00));
    }

    public boolean containsCurrencyBalance(currency currency) {
        return balance.containsKey(currency);
    }

    public void putBalance(User.currency currency, BigDecimal balance) {
        this.balance.put(currency, balance);
    }

    private User(Builder builder) {
        this.name = builder.name;
        this.userName = builder.userName;
        this.balance = builder.balance;
    }

    public static class Builder {

        private String name;
        private String userName;
        private Map<User.currency, BigDecimal> balance;

        public static Builder newInstance() {
            return new Builder();
        }

        private Builder() {}

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setUserName(String userName) {
            this.userName = userName;
            return this;
        }

        public Builder putBalance(User.currency currency, BigDecimal balance) {
            // When adding the first currency balance, the map isn't instantiated
            if (this.balance == null) {
                this.balance = new HashMap<>();
            }
            this.balance.put(currency, balance);
            return this;
        }

        public User build() {
            return new User(this);
        }
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", userName='" + userName + '\'' +
                ", balance=" + balance +
                '}';
    }
}
