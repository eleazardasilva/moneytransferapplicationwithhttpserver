package com.eleazar.backend.example.repository;

import com.eleazar.backend.example.entity.Transaction;
import com.eleazar.backend.example.entity.User;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * Created by Eleazar da Silva on 11-11-2022.
 * Simulates the interaction with a db
 */
public class InMemoryDataStore implements PersistenceInterface {

    private static InMemoryDataStore inMemoryDataStore;
    /**
     * initial capacity added to improve performance.
     * Collections' capacity is changed dynamically when needed
     * By setting an approximate value we spare additional processing
     * <p>
     * Singleton pattern used to simulate a real database interaction.
     */
    private ArrayList<User> users = new ArrayList<>(15);
    private ArrayList<Transaction> transactions = new ArrayList<>(100);

    private InMemoryDataStore() {
        // dummy data to simulate existing records on database
        loadUsers();
        loadTransactions();
    }

    public static InMemoryDataStore getInstance() {
        if (inMemoryDataStore == null) {
            inMemoryDataStore = new InMemoryDataStore();
        }
        return inMemoryDataStore;
    }

    private void loadUsers() {

        User user1 = User.Builder.newInstance().setName("Eleazar da Silva").setUserName("eleazarSilva")
                .putBalance(User.currency.EUR, new BigDecimal(22.30).setScale(2, RoundingMode.HALF_DOWN))
                .build();
        User user2 = User.Builder.newInstance().setName("Maria Afra").setUserName("mariaA")
                .putBalance(User.currency.EUR, new BigDecimal(220.30).setScale(2, RoundingMode.HALF_DOWN))
                .putBalance(User.currency.USD, new BigDecimal(220.30).setScale(2, RoundingMode.HALF_DOWN))
                .build();
        User user3 = User.Builder.newInstance().setName("Jon Silver").setUserName("jSilver")
                .putBalance(User.currency.GBD, new BigDecimal(22.30).setScale(2, RoundingMode.HALF_DOWN))
                .build();
        User user4 = User.Builder.newInstance().setName("Joana Maio").setUserName("Joana")
                .putBalance(User.currency.USD, new BigDecimal(209.00).setScale(2, RoundingMode.HALF_DOWN))
                .putBalance(User.currency.EUR, new BigDecimal(500.99).setScale(2, RoundingMode.HALF_DOWN))
                .build();
        User user5 = User.Builder.newInstance().setName("Carlos").setUserName("carlos123")
                .putBalance(User.currency.BTC, new BigDecimal(1).setScale(9, RoundingMode.HALF_DOWN))
                .putBalance(User.currency.USD, new BigDecimal(450.09).setScale(2, RoundingMode.HALF_DOWN))
                .putBalance(User.currency.JPY, new BigDecimal(50.09).setScale(2, RoundingMode.HALF_DOWN))
                .putBalance(User.currency.EUR, new BigDecimal(344.32).setScale(2, RoundingMode.HALF_DOWN))
                .build();
        User user6 = User.Builder.newInstance().setName("Matt Lem").setUserName("mattLem")
                .putBalance(User.currency.USD, new BigDecimal(450.09).setScale(2, RoundingMode.HALF_DOWN))
                .putBalance(User.currency.EUR, new BigDecimal(500.99).setScale(2, RoundingMode.HALF_DOWN))
                .build();

        users.add(user1);
        users.add(user2);
        users.add(user3);
        users.add(user4);
        users.add(user5);
        users.add(user6);
    }

    private void loadTransactions() {
        Transaction transaction1 = Transaction.Builder.newInstance()
                .setTransactionId(0)
                .setUserNameFrom("eleazarSilva")
                .setUserNameTo("jSilver")
                .setCurrency(User.currency.EUR)
                .setAmount(new BigDecimal(500).setScale(2, RoundingMode.HALF_DOWN))
                .setTimestamp("10/03/2003")
                .build();

        Transaction transaction2 = Transaction.Builder.newInstance()
                .setTransactionId(1)
                .setUserNameFrom("Joana")
                .setUserNameTo("jSilver")
                .setCurrency(User.currency.EUR)
                .setAmount(new BigDecimal(220).setScale(2, RoundingMode.HALF_DOWN))
                .setTimestamp("10/03/2003")
                .build();

        transactions.add(transaction1);
        transactions.add(transaction2);
    }

    @Override
    public synchronized void insertUserAndCommit(String name, String userName) {
        User user = User.Builder.newInstance().setName(name).setUserName(userName)
                .build();
        users.add(user);
    }

    @Override
    public User findUserByUserName(String userName) {
        Optional optional = users.stream()
                .parallel()
                .filter(a -> a.getUserName().equals(userName)).distinct().findFirst();
        if (optional.isPresent()) {
            return (User) optional.get();
        } else {
            throw new NoSuchElementException("User not found");
        }
    }

    public boolean findUserExists(String userName) {
        return users.stream().parallel().filter(a -> a.getUserName().equals(userName)).distinct().count() > 0;
    }

    @Override
    public Transaction findTransactionById(int transactionId) {
        Optional optional = transactions.stream()
                .parallel()
                .filter(a -> a.getTransactionId() == transactionId).distinct().findFirst();
        if (optional.isPresent()) {
            return (Transaction) optional.get();
        } else {
            throw new NoSuchElementException("Transaction not available.");
        }
    }

    @Override
    public BigDecimal findUserCurrencyBalance(String userIn, User.currency currency) {
        Optional optional = users.stream()
                .parallel()
                .filter(a -> a.getUserName().equals(userIn)).distinct().findFirst();
        if (optional.isPresent()) {
            User user = (User) optional.get();
            return user.getBalance(currency);
        } else {
            throw new NoSuchElementException("User don't have balance in this currency.");
        }
    }

    @Override
    public synchronized boolean updateAndCommit(ArrayList<User> transactionUsers, Transaction transaction) {
        // commit user
        for (User user : transactionUsers) {
            users.replaceAll(a -> {
                if (a.getUserName().equals(user.getUserName())) return user;
                return a;
            });
        }
        int id = transactions.size();
        transaction.setTransactionId(id);
        // commit transaction
        transactions.add(transaction);
        return true;
    }

    @Override
    public synchronized ArrayList<Transaction> findUserTransactions(String userName) {
        return transactions.stream().parallel()
                .collect(() -> new TransactionCollector(userName), TransactionCollector::accumulate, TransactionCollector::combine)
                .getResult();
    }
}

/**
 * customised collector class for the stream
 */
class TransactionCollector {

    private ArrayList<Transaction> transactions = new ArrayList<>();
    private String userName;

    TransactionCollector(String userName) {
        this.userName = userName;
    }

    ArrayList<Transaction> getResult() {
        return transactions;
    }

    void accumulate(Transaction transaction) {
        // Get transaction of the user. To and From
        if (transaction.getUserNameFrom().equals(userName) | transaction.getUserNameTo().equals(userName)) {
            transactions.add(transaction);
        }
    }

    void combine(TransactionCollector other) {
        transactions.addAll(other.transactions);
    }
}