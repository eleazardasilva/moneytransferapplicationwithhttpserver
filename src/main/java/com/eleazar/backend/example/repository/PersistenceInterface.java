package com.eleazar.backend.example.repository;

import com.eleazar.backend.example.entity.Transaction;
import com.eleazar.backend.example.entity.User;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Created by Eleazar da Silva on 27-01-2019.
 * Defines the data persistence contract
 */
public interface PersistenceInterface {

    User findUserByUserName(String userName);

    BigDecimal findUserCurrencyBalance(String userName, User.currency currency);

    ArrayList<Transaction> findUserTransactions(String userName);

    void insertUserAndCommit(String name, String userName);

    boolean findUserExists(String userName);

    Transaction findTransactionById(int transactionId);

    // commit can involve one of more users on a transaction
    boolean updateAndCommit(ArrayList<User> users, Transaction transaction);
}
