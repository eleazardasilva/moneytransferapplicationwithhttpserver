package com.eleazar.backend.example.service;

import com.eleazar.backend.example.entity.Transaction;
import com.eleazar.backend.example.entity.TransactionReply;
import com.eleazar.backend.example.entity.User;
import com.eleazar.backend.example.repository.InMemoryDataStore;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Eleazar da Silva on 11-11-2022.
 *
 */
public class TransactionProcessor {

    private InMemoryDataStore dataStore;

    public TransactionProcessor(){
        dataStore = InMemoryDataStore.getInstance();
    }

    public BigDecimal getUserBalance(String userName, User.currency currency) {
        return dataStore.findUserCurrencyBalance(userName, currency);
    }

    public User getUserInfo(String userName) {
        return dataStore.findUserByUserName(userName);
    }

    public ArrayList<Transaction> getUserTransactions(String userName) {
        return dataStore.findUserTransactions(userName);
    }

    // handles money transference
    public synchronized TransactionReply processTransference(Transaction transaction) {

        String userFrom = transaction.getUserNameFrom();
        String userTo = transaction.getUserNameTo();
        BigDecimal amount = transaction.getAmount();
        User debitUser;
        User creditUser;
        TransactionReply reply;

        // validate users and balance and attempts to processTransference the transaction
        if (invalidValidUser(userFrom)) {
            reply = TransactionReply.Builder.newInstance()
                    .setCode(3)
                    .setDescription("Debit user not found")
                    .build();
        } else if (invalidValidUser(userTo)) {
            reply = TransactionReply.Builder.newInstance()
                    .setCode(4)
                    .setDescription("Credit user not found")
                    .build();
        } else if (!isBalanceAvailable(userFrom, amount, transaction.getCurrency())) {
            reply = TransactionReply.Builder.newInstance()
                    .setCode(5)
                    .setDescription("Balance unavailable for the transaction")
                    .build();
        } else {
            debitUser = debitAccount(userFrom, amount, transaction.getCurrency());
            creditUser = creditAccount(userTo, amount, transaction.getCurrency());
            // commit
            dataStore.updateAndCommit(new ArrayList<>(Arrays.asList(debitUser, creditUser)), transaction);
            // write reply after commit confirmed
            reply = TransactionReply.Builder.newInstance()
                    .setCode(0)
                    .setDescription("Success")
                    .build();
        }

        return reply;
    }

    private boolean invalidValidUser(String userIn) {
        return !dataStore.findUserExists(userIn);
    }

    private boolean isBalanceAvailable(String userFrom, BigDecimal amount, User.currency currency) {

        BigDecimal balance = dataStore.findUserCurrencyBalance(userFrom, currency);
        return (balance.subtract(amount)).compareTo(new BigDecimal(0.00)) > 0;
    }

    private User creditAccount(String userName, BigDecimal amount, User.currency currency) {
        User user = dataStore.findUserByUserName(userName);
        // recalculate balance.
        BigDecimal newBalance = user.containsCurrencyBalance(currency) ?
                user.getBalance(currency).add(amount)
                :amount;
        // sets user with new balance
        user.putBalance(currency, newBalance);

        // some validation should be done here
        return user;
    }

    private User debitAccount(String userName, BigDecimal amount, User.currency currency) {
        // user and transaction have to be global to allow transactional behaviour
        User user = dataStore.findUserByUserName(userName);
        user.putBalance(currency, user.getBalance(currency).subtract(amount));

        // some validation should be done here
        return user;
    }
}
