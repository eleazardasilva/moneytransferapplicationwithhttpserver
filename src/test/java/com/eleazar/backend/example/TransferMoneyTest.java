package com.eleazar.backend.example;

import com.eleazar.backend.example.entity.Transaction;
import com.eleazar.backend.example.entity.TransactionReply;
import com.eleazar.backend.example.entity.User;
import com.eleazar.backend.example.repository.InMemoryDataStore;
import com.eleazar.backend.example.service.TransactionProcessor;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import resources.MyJUnitStopWatch;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

import static org.junit.Assert.*;

/**
 * Created by Eleazar da Silva on 11-11-2022.
 *
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TransferMoneyTest {

    @Rule
    public MyJUnitStopWatch stopwatch = new MyJUnitStopWatch();

    private InMemoryDataStore dataStore = InMemoryDataStore.getInstance();

    // Repository tests
    @Test
    public void test001getUser(){
        User user = dataStore.findUserByUserName("eleazarSilva");
        assertEquals("Get user info","User{name='Eleazar da Silva', userName='eleazarSilva', balance={EUR=22.30}}",
                user.toString());
    }

    @Test
    public void test002getUserCurrencyBalance() {
        BigDecimal balance = dataStore.findUserCurrencyBalance("eleazarSilva", User.currency.EUR);
        assertEquals("Get user balance of a currency","22.30",balance.toString());
    }

    @Test
    public void test003getUserTransactions() {
        ArrayList<Transaction> transactions = dataStore.findUserTransactions("eleazarSilva");
        assertEquals("Get list of transactions","Transaction{transactionId=0, userNameFrom='eleazarSilva', userNameTo='jSilver', amount=500.00, currency=EUR, timestamp='10/03/2003'}",transactions.get(0).toString());
    }

    @Test
    public void test004addUserAndValidate() {
        dataStore.insertUserAndCommit("Manuel Ortega", "m.ortega");
        boolean userExists = dataStore.findUserExists("m.ortega");
        assertTrue("Test add and validation of user", userExists);
    }

    @Test
    public void test005getTransaction() {
        Transaction transaction = dataStore.findTransactionById(1);
        assertEquals("Get transaction by id","Transaction{transactionId=1, userNameFrom='Joana', userNameTo='jSilver', amount=220.00, currency=EUR, timestamp='10/03/2003'}",transaction.toString());
    }

    @Test
    public void test006makeTransactionAndUpdateDB() {
        User userFrom = dataStore.findUserByUserName("eleazarSilva");
        User userTo = dataStore.findUserByUserName("Joana");
        BigDecimal amount = new BigDecimal(400).setScale(2, RoundingMode.HALF_DOWN);

        // debit
        userFrom.putBalance(User.currency.EUR, userFrom.getBalance(User.currency.EUR).subtract(amount));

        // credit
        BigDecimal newBalance = userTo.containsCurrencyBalance(User.currency.EUR) ?
                userTo.getBalance(User.currency.EUR).add(amount)
                :amount;
        // sets user with new balance
        userTo.putBalance(User.currency.EUR, newBalance);

        Transaction transaction = Transaction.Builder.newInstance()
                .setUserNameFrom(userFrom.getUserName())
                .setUserNameTo(userTo.getUserName())
                .setAmount(amount)
                .setCurrency(User.currency.EUR)
                .setTimestamp("11/11/2022")
                .build();

        // update and commit changes
        boolean result = dataStore.updateAndCommit(new ArrayList<>(Arrays.asList(userFrom, userTo)), transaction);

        Transaction newTransaction = dataStore.findTransactionById(2);

        assertTrue("Get user balance of a currency", result);
        assertEquals("Get user balance of a currency","Transaction{transactionId=2, userNameFrom='eleazarSilva', userNameTo='Joana', amount=400.00, currency=EUR, timestamp='11/11/2022'}",newTransaction.toString());
    }

    @Test
    public void test007getUserErrorNoSuchElementException(){
        User user = null;
        try {
            user = dataStore.findUserByUserName("eleazar1Silva");
        } catch (Exception e) {
            assertEquals("User not found", e.getMessage());
        }
        assertNull("Exception treated and null returned", user);
    }

    @Test
    public void test008getUserCurrencyBalanceNoSuchElementException() {
        BigDecimal balance = null;
        try {
            balance = dataStore.findUserCurrencyBalance("eleazar", User.currency.EUR);
            assertEquals("Get user balance of a currency","22.30",balance.toString());
        } catch (Exception e) {
            assertEquals("User don't have balance in this currency.", e.getMessage());
        }
        assertNull("Exception treated and null returned", balance);
    }

    @Test
    public void test009getTransactionException() {
        Transaction transaction = null;
        try {
            transaction = dataStore.findTransactionById(4);
        } catch (Exception e) {
            assertEquals("Transaction not available.", e.getMessage());
        }
        assertNull("Exception treated and null returned", transaction);
    }

    @Test
    public void test010processTransference() {
        TransactionProcessor processor = new TransactionProcessor();
        TransactionReply reply = null;
        Transaction transaction = Transaction.Builder.newInstance()
                .setUserNameFrom("carlos123")
                .setUserNameTo("Joana")
                .setCurrency(User.currency.USD)
                .setAmount(new BigDecimal(70).setScale(2, RoundingMode.HALF_DOWN))
                .setTimestamp("14/11/2022")
                .build();
        try {
            reply = processor.processTransference(transaction);
        } catch (Exception e) {
            assertEquals("Test exception on full transaction", "Transaction not available.", e.getMessage());
        }
        assertEquals("Test a full transaction","TransactionReply{code=0, description='Success'}", reply.toString());
    }

    @Test
    public void test011processTransferenceBalanceUnavailable() {
        TransactionProcessor processor = new TransactionProcessor();
        TransactionReply reply = null;
        Transaction transaction = Transaction.Builder.newInstance()
                .setUserNameFrom("eleazarSilva")
                .setUserNameTo("Joana")
                .setCurrency(User.currency.USD)
                .setAmount(new BigDecimal(70).setScale(2, RoundingMode.HALF_DOWN))
                .setTimestamp("14/11/2022")
                .build();
        try {
            reply = processor.processTransference(transaction);
        } catch (Exception e) {
            assertEquals("Test exception on full transaction", "Transaction not available.", e.getMessage());
        }
        assertEquals("Test a full transaction","TransactionReply{code=5, description='Balance unavailable for the transaction'}", reply.toString());
    }

    @Test
    public void test012processTransferenceInvalidDebitUser() {
        TransactionProcessor processor = new TransactionProcessor();
        TransactionReply reply = null;
        Transaction transaction = Transaction.Builder.newInstance()
                .setUserNameFrom("elazarrSilver")
                .setUserNameTo("Joana")
                .setCurrency(User.currency.USD)
                .setAmount(new BigDecimal(70).setScale(2, RoundingMode.HALF_DOWN))
                .setTimestamp("14/11/2022")
                .build();
        try {
            reply = processor.processTransference(transaction);
        } catch (Exception e) {
            assertEquals("Test exception on full transaction", "Transaction not available.", e.getMessage());
        }
        assertEquals("Test a full transaction","TransactionReply{code=3, description='Debit user not found'}", reply.toString());
    }

    @Test
    public void test013processTransferenceInvalidCreditUser() {
        TransactionProcessor processor = new TransactionProcessor();
        TransactionReply reply = null;
        Transaction transaction = Transaction.Builder.newInstance()
                .setUserNameFrom("carlos123")
                .setUserNameTo("JoanaLana")
                .setCurrency(User.currency.USD)
                .setAmount(new BigDecimal(70).setScale(2, RoundingMode.HALF_DOWN))
                .setTimestamp("14/11/2022")
                .build();
        try {
            reply = processor.processTransference(transaction);
        } catch (Exception e) {
            assertEquals("Test exception on full transaction", "Transaction not available.", e.getMessage());
        }
        assertEquals("Test a full transaction","TransactionReply{code=4, description='Credit user not found'}", reply.toString());
    }

    @Test
    public void test014processTransferenceCurrencyUnavailable() {
        TransactionProcessor processor = new TransactionProcessor();
        TransactionReply reply = null;
        Transaction transaction = Transaction.Builder.newInstance()
                .setUserNameFrom("carlos123")
                .setUserNameTo("Joana")
                .setCurrency(User.currency.CAD)
                .setAmount(new BigDecimal(70).setScale(2, RoundingMode.HALF_DOWN))
                .setTimestamp("14/11/2022")
                .build();
        try {
            reply = processor.processTransference(transaction);
        } catch (Exception e) {
            assertEquals("Test exception on full transaction", "Transaction not available.", e.getMessage());
        }
        assertEquals("Test a full transaction","TransactionReply{code=5, description='Balance unavailable for the transaction'}", reply.toString());
    }

    @Test
    public void test015processTransferenceConcurrentTransactions() {
        TransactionProcessor processor = new TransactionProcessor();

        BigDecimal balanceFirstUser = processor.getUserBalance("mattLem", User.currency.USD);
        BigDecimal balanceSecondUser = processor.getUserBalance("mariaA", User.currency.USD);
        System.out.println("initial balance for first user "+balanceFirstUser);
        System.out.println("initial balance for second user "+balanceSecondUser);

        // create two runnable blocks with multiple operations
        Runnable transactionTask1 = () -> {
            for (int i = 0; i < 20; i++) {
                Transaction transaction = Transaction.Builder.newInstance()
                        .setUserNameFrom("mattLem")
                        .setUserNameTo("mariaA")
                        .setCurrency(User.currency.USD)
                        .setAmount(new BigDecimal(7).setScale(2, RoundingMode.HALF_DOWN))
                        .setTimestamp("14/11/2022")
                        .build();
                processor.processTransference(transaction);
                try {
                    Thread.sleep(ThreadLocalRandom.current().nextLong(300));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        Runnable transactionTask2 = () -> {
            for (int i = 0; i <= 20; i++) {
                Transaction transaction = Transaction.Builder.newInstance()
                        .setUserNameFrom("mariaA")
                        .setUserNameTo("mattLem")
                        .setCurrency(User.currency.USD)
                        .setAmount(new BigDecimal(3).setScale(2, RoundingMode.HALF_DOWN))
                        .setTimestamp("14/11/2022")
                        .build();
                processor.processTransference(transaction);
                try {
                    Thread.sleep(ThreadLocalRandom.current().nextLong(300));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        Thread thread1 = new Thread(transactionTask1);
        Thread thread2 = new Thread(transactionTask2);

        thread1.start();
        thread2.start();

        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        balanceFirstUser = processor.getUserBalance("mattLem", User.currency.USD);
        balanceSecondUser = processor.getUserBalance("mariaA", User.currency.USD);
        System.out.println("final balance for first user "+balanceFirstUser);
        System.out.println("final balance for second user "+balanceSecondUser);
        assertEquals("Make concurrent transactions and validate final balance","373.09 and 297.30",
                balanceFirstUser.toString()+" and "+balanceSecondUser.toString());
     }
}
