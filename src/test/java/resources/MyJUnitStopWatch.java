package resources;

import org.junit.AssumptionViolatedException;
import org.junit.rules.Stopwatch;
import org.junit.runner.Description;

import java.util.concurrent.TimeUnit;

/**
 * Created by Eleazar da Silva on 12-10-2016.
 *
 */

public class MyJUnitStopWatch extends Stopwatch{

    private static void logInfo(Description description, String status, long nanos) {
        String testName = description.getMethodName();
        System.out.println(String.format("Test %s %s, spent %d milliseconds",
                testName, status, TimeUnit.NANOSECONDS.toMicros(nanos/1000)));
    }

    @Override
    protected void succeeded(long nanos, Description description) {
        logInfo(description, "Succeeded", nanos);
    }

    @Override
    protected void failed(long nanos, Throwable e, Description description) {
        logInfo(description, "Failed", nanos);
    }

    @Override
    protected void skipped(long nanos, AssumptionViolatedException e, Description description) {
        logInfo(description, "Skipped", nanos);
    }

/*    @Override
    protected void finished(long nanos, Description description) {
        logInfo(description, "Finished", nanos);
    }*/
}